$(document).ready(function() {
 
    $('.select2').select2();
    var form ='<div class="delete">'+
                  '<div class="form-group col-md-3">'+
                    '<label for="inputAddress">Mối quan hệ </label> ' + 
                    '<input type="text" class="form-control" id="quanhe" placeholder="Cha, mẹ, vợ,…"></div>'+
                    '<div class="form-group col-md-4">'+
                    '<label for="inputAddress"> Họ và tên</label>'+
                    '<input type="text" class="form-control" id="hoten" placeholder="Họ và tên"></div>'+
                  
                    '<div class="form-group col-md-4">'+
                    '<label for="inputAddress">Địa chỉ liên hệ</label>'+
                    '<input type="text" class="form-control" id="diachilh" placeholder="Địa chỉ liên hệ">'+
                  '</div>'+
                 
                  '<button type="button" id="removemoiquanhe" class="btn plus"><i class="fa fa-minus"></i></button>'+
                '</div>'
	$('#addmoiquanhe').click(function(){
		 $(".mqh").append(form);
	})
	$('body').on('click','#removemoiquanhe',function(){
    $(this).parents('.delete').remove();
  });

    $('form').submit(function(e){
      const nhansu={};
          nhansu.ma= $("#maNV").val();
          nhansu.ho=$("#ho").val();
          nhansu.ten=$("#ten").val();
          nhansu.ngaysinh=$("#ngaysinh").val();
          nhansu.quoctich=$("#quoctich").val();
          nhansu.socmt=$("#socmt").val();
          nhansu.email=$("#email").val();
        
          var error=1;
          if(nhansu.ma == ""){
            $("#errorMa").html("Vui lòng nhập mã NV");
            error=1;
          }
          else{
              $("#errorMa").html("");
              error=0;  
          }
          if(nhansu.ten == ""){
            $("#errorTen").html("Vui lòng nhập tên ");
            error=1;
          }
          else{
              $("#errorTen").html("");
              error=0;
          }
         if(nhansu.ho == ""){
            $("#errorHo").html("Vui lòng nhập họ");
            error=1;
          }
          else{
              $("#errorHo").html("");
              error=0; 
          } 
          if(nhansu.ngaysinh == ""){
            $("#errorNS").html("Vui lòng nhập ngày sinh");
            error=1; 
          }
          else{
              $("#errorNS").html("");
              error=0; 
          }
         if(nhansu.quoctich == ""){
            $("#errorQT").html("Vui lòng nhập quốc tịch");
            error=1;
         }
          else{
              $("#errorQT").html("");
              error=0;
          }
          if(nhansu.socmt == ""){
            $("#errorCMT").html("Vui lòng nhập số CMND");
            error=1;
          }
         else{
              $("#errorCMT").html("");
              error=0; 
          }
          if(nhansu.email == "" ){
            $("#errorMail").html("Vui lòng nhập email");
           error=1;
          }
         else{
               $("#errorMail").html("");
              error=0;
          }
         if(error==0)
            return true;
         else{
            e.preventDefault();
            alert("Vui lòng nhập vào các trường bắt buộc !")
            return false;
       }
      
   

    })
    var quocgia= $('#quocgia');
    var tinhTP= $('#tinh');
    var quanHuyen= $('#quanhuyen');
    var phuongXa= $('#phuongxa');

  function getDiaChi(url,select,name,ID){
    
    $.get(url, function(data){
      select.append('<option value="">Chọn</option>'); 
      return  data.map(function(data) {
        select.append('<option value='+data[ID]+">"+data[name]+'</option>'); 
        
    });
  })
}

   getDiaChi("http://ums-dev.husc.edu.vn/api/tudiendulieu/quocgia/list",quocgia,'TenQuocGia','MaQuocGia');
   getDiaChi("http://ums-dev.husc.edu.vn/api/tudiendulieu/tinhthanhpho/list/001/",tinhTP,'TenTinhThanhPho','MaTinhThanhPho');
   getDiaChi('http://ums-dev.husc.edu.vn/api/tudiendulieu/quanhuyen/list/01/',quanHuyen,'TenQuanHuyen','MaQuanHuyen');
   getDiaChi('http://ums-dev.husc.edu.vn/api/tudiendulieu/phuongxa/list/001/',phuongXa,'TenPhuongXa','MaPhuongXa'); 
   
   quocgia.change(function(){ // $('body').on('change','#quocgia',function(){
     getDiaChi('http://ums-dev.husc.edu.vn/api/tudiendulieu/tinhthanhpho/list/'+$(this).val()+'/',tinhTP,'TenTinhThanhPho','MaTinhThanhPho');
     console.log($(this).val());
     tinhTP.empty();
   })

   tinhTP.change(function(){ 
     getDiaChi('http://ums-dev.husc.edu.vn/api/tudiendulieu/quanhuyen/list/'+$(this).val()+'/',quanHuyen,'TenQuanHuyen','MaQuanHuyen');
     console.log($(this).val());
     quanHuyen.empty();
   })

   quanHuyen.change(function(){ 
     getDiaChi('http://ums-dev.husc.edu.vn/api/tudiendulieu/phuongxa/list/'+$(this).val()+'/',phuongXa,'TenPhuongXa','MaPhuongXa');
     console.log($(this).val());
     phuongXa.empty();
   })

})



